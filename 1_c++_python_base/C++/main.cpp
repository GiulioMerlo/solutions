#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

/// \brief ImportText import the text for encryption
/// \param inputFilePath: the input file path
/// \param text: the resulting text
/// \return the result of the operation, true is success, false is error
bool ImportText(const string& inputFilePath,
                string& text);

/// \brief Encrypt encrypt the text
/// \param text: the text to encrypt
/// \param password: the password for encryption
/// \param encryptedText: the resulting encrypted text
/// \return the result of the operation, true is success, false is error
bool Encrypt(const string& text,
             const string& password,
             string& encryptedText);

/// \brief Decrypt decrypt the text
/// \param text: the text to decrypt
/// \param password: the password for decryption
/// \param decryptedText: the resulting decrypted text
/// \return the result of the operation, true is success, false is error
bool Decrypt(const string& text,
             const string& password,
             string& decryptedText);

int main(int argc, char** argv)
{
  if (argc < 2)
  {
    cerr<< "Password shall passed to the program"<< endl;
    return -1;
  }
  string password = argv[1];

  string inputFileName = "./text.txt", text;
  if (!ImportText(inputFileName, text))
  {
    cerr<< "Something goes wrong with import"<< endl;
    return -1;
  }
  else
    cout<< "Import successful: result= "<< text<< endl;

  string encryptedText;
  if (!Encrypt(text, password, encryptedText))
  {
    cerr<< "Something goes wrong with encryption"<< endl;
    return -1;
  }
  else
    cout<< "Encryption successful: result= "<< encryptedText<< endl;

  string decryptedText;
  if (!Decrypt(encryptedText, password, decryptedText) || text != decryptedText)
  {
    cerr<< "Something goes wrong with decryption"<< endl;
    return -1;
  }
  else
    cout<< "Decryption successful: result= "<< decryptedText<< endl;

  return 0;
}

bool ImportText(const string& inputFilePath,
                string& text)
{
  ifstream file;
  //open, read and close the input file
  file.open(inputFilePath);
  getline(file, text);
  file.close();

  return true;
}

bool Encrypt(const string& text,
             const string& password,
             string& encryptedText)
{
  unsigned int k = 0;
  unsigned int n = 0;
  k = password.length(); //acquire length of the password
  n = text.length();     //acquire length of the text

  encryptedText = text;  //initialize new string to the same length and content of text

  //encryption cicle
  for(unsigned int i = 0; i < n; i++){
    encryptedText[i] = (unsigned int)encryptedText[i] + ((unsigned int)password[i%k]- 64);
  }

  //exit encryption
  return true;
}

bool Decrypt(const string& text,
             const string& password,
             string& decryptedText)
{

    unsigned int k = 0;
    unsigned int n = 0;
    k = password.length(); //acquire length of the password
    n = text.length();     //acquire length of text

    decryptedText = text;  //initialize new string to the same length and content of text

    //decryption cicle
    for(unsigned int i = 0; i < n; i++){
      decryptedText[i] = (unsigned int)decryptedText[i] - ((unsigned int)password[i%k]- 64);
     }

    //exit decryption
    return  true;
}
