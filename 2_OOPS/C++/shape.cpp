#include "shape.h"
#include <math.h>
#define PI 3.141592653589793
#define Q(n) (n)*(n)

namespace ShapeLibrary {

Point::Point(const double &x, const double &y)
{
    X = x;
    Y = y;
}

Point::Point(const Point &point)
{
    X = point.X;
    Y = point.Y;
}

Ellipse::Ellipse(const Point &center, const int &a, const int &b)
{
    C = center;
    A = a;
    B = b;
}

Ellipse::Ellipse()
{
    C = Point();
    A = 0;
    B = 0;
}

double Ellipse::Area() const
{
    return A*B*PI;
}

Circle::Circle(const Point &center, const int &radius)
{
    C = center;
    A = radius;
    B = radius;
}

Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3)
{
    A = p1;
    B = p2;
    C = p3;
}

Triangle::Triangle()
{
    A = Point();
    B = Point();
    C = Point();
}

double Triangle::Area() const
{
    double AB = sqrt(Q(A.X - B.X) + Q(A.Y - B.Y));
    double AC = sqrt(Q(A.X - C.X) + Q(A.Y - C.Y));
    double BC = sqrt(Q(B.X - C.X) + Q(B.Y - C.Y));
    double P = 0.5 * (AB + BC + AC);
    //formula di Erone
    return sqrt(P * (P-AB) * (P - BC) * (P - AC));
}

TriangleEquilateral::TriangleEquilateral(const Point &p1, const int &edge)
{
    A = p1;
    L = edge;
}

double TriangleEquilateral::Area() const
{
    return 0.5 * L * L * 0.5 * sqrt(3);
}

Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
{
    A = p1;
    B = p2;
    C = p3;
    D = p4;
}

double Quadrilateral::Area() const
{
//  Ho diviso il quadrilatero in due triangoli (ABD e BCD) e calcolato le due aree con la formula di Erone
//  Per fare ciò ho supposto il quadrilatero convesso e i punti dati in modo da formare segmenti consecutivi e tutti disgiunti
//    Triangle* t1;
//    Triangle* t2;
//    t1 = new Triangle(A,B,D);
//    t2 = new Triangle(B, C, D);
//    double Area = t1->Area() + t2->Area();
//    delete t1;
//    delete t2;
//    return Area;
    return Triangle(A, B, D).Area() + Triangle(B, C, D).Area();
}

Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p4)
{
    A = p1;
    B = p2;
    C = p4;
}

double Parallelogram::Area() const
{
    return 2 * Triangle(A, B, C).Area();
    //Raddoppio l'area del triangolo descritto dai tre punti
}

Rectangle::Rectangle(const Point &p1, const int &base, const int &height)
{
    A = p1;
    B = base;
    H = height;
}

Rectangle::Rectangle()
{
    A = Point();
    B = 0;
    H = 0;
}

double Rectangle::Area() const
{
    return (double)B*H;
}

Square::Square(const Point &p1, const int &edge)
{
    A = p1;
    B = edge;
    H = edge;
}

}
