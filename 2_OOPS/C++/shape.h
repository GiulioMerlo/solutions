#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>

using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double X;
      double Y;

      Point(const double& x,
            const double& y);

      Point(){X = 0.0; Y = 0.0;}

      Point(const Point& point);
  };

  class IPolygon {
    public:
      virtual double Area() const = 0;
  };

  class Ellipse : public IPolygon
  {
    public:
      Point C;
      int A;
      int B;
      Ellipse(const Point& center,
              const int& a,
              const int& b);

      Ellipse();

      double Area() const;
  };

  class Circle : public Ellipse
  {
    public:
      Circle(const Point& center,
             const int& radius);
  };


  class Triangle : public IPolygon
  {
    public:
      Point A;
      Point B;
      Point C;

      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      Triangle();

      double Area() const;
  };


  class TriangleEquilateral : public IPolygon
  {
    public:
      int L = 0;
      Point A;

      TriangleEquilateral(const Point& p1,
                          const int& edge);

      double Area() const;
  };

  class Quadrilateral : public IPolygon
  {
    public:
      Point A;
      Point B;
      Point C;
      Point D;

      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);

      double Area() const;
  };


  class Parallelogram : public IPolygon
  {
    public:
      Point A;
      Point B;
      Point C;

      Parallelogram(const Point& p1,
                    const Point& p2,
                    const Point& p4);

      double Area() const;
  };

  class Rectangle : public IPolygon
  {
    public:
      Point A;
      int B = 0;
      int H = 0;

      Rectangle(const Point& p1,
                const int& base,
                const int& height);

      Rectangle();

      double Area() const;
  };

  class Square: public Rectangle
  {
    public:
      Square(const Point& p1,
             const int& edge);
  };
}

#endif // SHAPE_H
