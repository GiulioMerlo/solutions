import math as m


class Point:
    def __init__(self, x: float, y: float):
        self.X = x
        self.Y = y


class IPolygon:
    def area(self) -> float:
        return 0.0


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self.C = center
        self.A = a
        self.B = b

    def area(self) -> float:
        return m.pi * self.A * self.B


class Circle(Ellipse):
    def __init__(self, center: Point, radius: int):
        super(Circle, self).__init__(center, radius, radius)
        self.C = center
        self.A = radius
        self.B = radius


class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self.A = p1
        self.B = p2
        self.C = p3

    def area(self) -> float:
        AB = m.sqrt((self.A.X - self.B.X) ** 2 + (self.A.Y - self.B.Y) ** 2)
        BC = m.sqrt((self.C.X - self.B.X) ** 2 + (self.C.Y - self.B.Y) ** 2)
        AC = m.sqrt((self.C.X - self.A.X) ** 2 + (self.C.Y - self.A.Y) ** 2)
        # formula di Erone
        p = 0.5 * (AB + BC + AC)
        return m.sqrt(p * (p - AB) * (p - BC) * (p - AC))


class TriangleEquilateral(IPolygon):
    def __init__(self, p1: Point, edge: int):
        self.A = p1
        self.L = edge

    def area(self) -> float:
        return (.5 ** 2) * m.sqrt(3) * self.L ** 2


class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self.A = p1
        self.B = p2
        self.C = p3
        self.D = p4

    def area(self) -> float:
        # Ho diviso il quadrilatero in due triangoli (ABD e BCD) e calcolato le due aree con la formula di Erone. Per
        # fare ciò ho supposto il quadrilatero convesso e i punti dati in modo da formare segmenti consecutivi e
        # tutti disgiunti
        return Triangle(self.A, self.B, self.D).area() + Triangle(self.B, self.C, self.D).area()


class Parallelogram(IPolygon):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        self.A = p1
        self.B = p2
        self.C = p4

    def area(self) -> float:
        return 2 * Triangle(self.A, self.B, self.C).area()


class Rectangle(IPolygon):
    def __init__(self, p1: Point, base: int, height: int):
        self.A = p1
        self.B = base
        self.H = height

    def area(self) -> float:
        return self.B * self.H


class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        super(Square, self).__init__(p1, edge, edge)
        self.A = p1
        self.B = edge
        self.H = edge
