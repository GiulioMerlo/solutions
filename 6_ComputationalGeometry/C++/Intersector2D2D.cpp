#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{

}


Intersector2D2D::~Intersector2D2D()
{

}
// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.row(0) = planeNormal;
    rightHandSide(0) = planeTranslation;

  return;
}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.row(1) = planeNormal;
    rightHandSide(1) = planeTranslation;
  return ;
}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
    if(abs(matrixNomalVector.row(0).cross(matrixNomalVector.row(1)).norm()) <= toleranceParallelism){
        if(abs(rightHandSide(0) - rightHandSide(1)) <= toleranceIntersection){
            intersectionType = Coplanar;
            return false;
        }
        else {
            intersectionType = NoInteresection;
            return false;
        }
    }
    else {
        intersectionType = LineIntersection;
        matrixNomalVector.row(2) = matrixNomalVector.row(0).cross(matrixNomalVector.row(1));
        tangentLine = matrixNomalVector.row(2);
        rightHandSide(2) = 0;
        pointLine = matrixNomalVector.colPivHouseholderQr().solve(rightHandSide);
        return true;
    }
  return false;
}
