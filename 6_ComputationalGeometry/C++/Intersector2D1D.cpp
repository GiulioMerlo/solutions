#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{

}
Intersector2D1D::~Intersector2D1D()
{

}
// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    planeNormalPointer = &planeNormal;
    planeTranslationPointer = &planeTranslation;
  return;
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
    lineOriginPointer = &lineOrigin;
    lineTangentPointer = &lineTangent;
  return;
}
// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{
    if (abs(planeNormalPointer->dot(*lineTangentPointer)) <= toleranceParallelism){
        if(abs(planeNormalPointer->dot(*lineOriginPointer) - *planeTranslationPointer) <= toleranceIntersection){
            intersectionType = Coplanar;
            return false;
        }
        else {
            intersectionType = NoInteresection;
            return false;
        }
    }
    else {
        intersectionParametricCoordinate = (*planeTranslationPointer-(planeNormalPointer->dot(*lineOriginPointer)))/planeNormalPointer->dot(*lineTangentPointer);
        return true;
    }


  return false;
}
