#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <vector>
#include <math.h>
using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double X;
      double Y;
      Point() {
          X = 0.0;
          Y = 0.0;
      }
      Point(const double& x,
            const double& y);
      Point(const Point& point);

      double ComputeNorm2() const;

      Point operator+(const Point& point) const;
      Point operator-(const Point& point) const;
      Point& operator-=(const Point& point);
      Point& operator+=(const Point& point);
      friend ostream& operator<<(ostream& stream, const Point& point)
      {
        stream << "Point: x = " + to_string((int) point.X) + " y = " + to_string((int) point.Y) + "\n";
        return stream;
      }
  };

  class IPolygon {
    public:
      virtual double Perimeter() const = 0;
      virtual void AddVertex(const Point& point) = 0;
      friend inline bool operator< (const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() < rhs.Perimeter(); }
      friend inline bool operator> (const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() > rhs.Perimeter(); }
      friend inline bool operator<=(const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() <= rhs.Perimeter(); }
      friend inline bool operator>=(const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() >= rhs.Perimeter(); }

  };

  class Ellipse : public IPolygon
  {
    protected:
      Point _center;
      double _a;
      double _b;

    public:
      Ellipse() {
          _center = Point();
          _a = 0;
          _b = 0;
      }
      Ellipse(const Point& center,
              const double& a,
              const double& b);
      virtual ~Ellipse() { }
      void AddVertex(const Point& point);
      void SetSemiAxisA(const double& a);
      void SetSemiAxisB(const double& b);
      double Perimeter() const;
  };

  class Circle : public Ellipse
  {
    public:
      Circle() {
          _center = Point();
          _a = 0.0;
          _b = 0.0;
      }
      Circle(const Point& center,
             const double& radius);
      virtual ~Circle() { }
  };


  class Triangle : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Triangle() {
          points.reserve(3);
      }
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      void AddVertex(const Point& point) {
          if (points.size() < 3){
              points.push_back(point);
          }
          else{
              throw runtime_error("Too many points for a triangle");
          }
      }
      double Perimeter() const;
  };


  class TriangleEquilateral : public Triangle
  {
    public:
      TriangleEquilateral(const Point& p1,
                          const double& edge);
      TriangleEquilateral(){
          points.reserve(3);
      }
  };

  class Quadrilateral : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Quadrilateral() {
          points.reserve(4);
      }
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);
      virtual ~Quadrilateral() { }
      void AddVertex(const Point& p) {
          if (points.size() < 4){
              points.push_back(p);
          }
          else{
              throw runtime_error("Too many points for a quadrilateral");
          }
      }

      double Perimeter() const;
  };

  class Rectangle : public Quadrilateral
  {
    public:
      Rectangle() {
          points.reserve(4);
      }
      Rectangle(const Point& p1,
                const Point& p2,
                const Point& p3,
                const Point& p4) {}
      Rectangle(const Point& p1,
                const double& base,
                const double& height);
      virtual ~Rectangle() { }
  };

  class Square: public Rectangle
  {
    public:
      Square() {
          points.reserve(4);
      }
      Square(const Point& p1,
             const Point& p2,
             const Point& p3,
             const Point& p4);
      Square(const Point& p1,
             const double& edge);
      virtual ~Square() { }
  };
}

#endif // SHAPE_H
