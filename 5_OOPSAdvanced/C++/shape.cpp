#include "shape.h"
#include <math.h>
#define PI 3.141592653589793

namespace ShapeLibrary {

Ellipse::Ellipse(const Point &center, const double &a, const double &b)
{
    _center = center;
    _a = a;
    _b = b;
}

void Ellipse::AddVertex(const Point &point)
{
    _center = point;
}

void Ellipse::SetSemiAxisA(const double &a)
{
    _a = a;
}

void Ellipse::SetSemiAxisB(const double &b)
{
    _b = b;
}

double Ellipse::Perimeter() const{
    return 2 * PI * sqrt((_a * _a + _b * _b)/2);
}

  Triangle::Triangle(const Point& p1,
                     const Point& p2,
                     const Point& p3)
  {
      points.resize(3);
      points[0] = p1;
      points[1] = p2;
      points[2] = p3;
  }

  double Triangle::Perimeter() const
  {
    if(points.size() <3){
        throw runtime_error("Not a Triangle. Missing at least a vertex");
    }
    double perimeter = 0;
    Point l;
    for (unsigned int i = 0; i < 3; i++){
        l = points[i] - points[(i+1)%3];
        perimeter += l.ComputeNorm2();
    }
    return perimeter;
  }

  TriangleEquilateral::TriangleEquilateral(const Point& p1,
                                           const double& edge)
  {
      points.resize(3);
      points[0] = p1;
      points[1] = Point(p1.X + edge, p1.Y);
      points[2] = Point(p1.X + edge * cos((double) PI/3), p1.Y + edge * sin((double) PI/3));
  }



  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4)
  {
      points.resize(4);
      points[0] = p1;
      points[1] = p2;
      points[2] = p3;
      points[3] = p4;
  }

  double Quadrilateral::Perimeter() const
  {
      if(points.size() < 4){
          throw runtime_error("Not a Quadrilteral. Missing at least a vertex");
      }
      double perimeter = 0;
      Point l;
      for (unsigned int i = 0; i < 4; i++){
          l = points[i] - points[(i+1)%4];
          perimeter += l.ComputeNorm2();
      }

    return perimeter;
  }

  Rectangle::Rectangle(const Point& p1,
                       const double& base,
                       const double& height)
  {
      points.resize(4);
      points[0] = p1;
      points[1] = p1 + Point(base, 0);
      points[2] = p1 + Point(base, height);
      points[3] = p1 + Point(0, height);
  }

  Point::Point(const double &x, const double &y)
  {
      X = x;
      Y = y;
  }

  Point::Point(const Point &point)
  {
      X = point.X;
      Y = point.Y;
  }

  double Point::ComputeNorm2() const
  {
      return sqrt(X*X + Y*Y);
  }

  Point Point::operator+(const Point& point) const
  {
      return Point(X + point.X, Y + point.Y);
  }

  Point Point::operator-(const Point& point) const
  {
      return Point(X - point.X, Y - point.Y);
  }

  Point&Point::operator-=(const Point& point)
  {
      X -= point.X;
      Y -= point.Y;
  }

  Point&Point::operator+=(const Point& point)
  {
      X += point.X;
      Y += point.Y;
  }

  Circle::Circle(const Point &center, const double &radius)
  {
      _center = center;
      _a = radius;
      _b = radius;
  }

  Square::Square(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
  {
      points.resize(4);
      points[0] = p1;
      points[1] = p2;
      points[2] = p3;
      points[3] = p4;
  }

  Square::Square(const Point &p1, const double &edge) {
      points.resize(4);
      points[0] = p1;
      points[1] = p1 + Point(edge, 0);
      points[2] = p1 + Point(edge, edge);
      points[3] = p1 + Point(0, edge);
  }

}
