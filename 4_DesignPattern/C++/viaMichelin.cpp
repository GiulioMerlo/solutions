# include "viaMichelin.h"

#include <iostream>
#include <fstream>
#include <sstream>

namespace ViaMichelinLibrary {
int RoutePlanner::BusAverageSpeed = 50;

void BusStation::Load() {
    Reset();
    ifstream file;
    file.open(_busFilePath.c_str());
    if (file.fail()){
        throw runtime_error("Something goes wrong");
    }
    try {
        string line = "";
        istringstream converter;
        getline(file, line); //comment
        getline(file, line);
        converter.str(line);
        converter >> _numberBuses;
        _buses.reserve(_numberBuses);
        converter.clear();

        getline(file, line); //comment
        getline(file, line); //comment

        int id, fuelCost;

        for (int i = 0; i < _numberBuses; i++){
            getline(file, line);
            converter.str(line);
            converter >> id >> fuelCost;
            converter.clear();
            _buses.push_back(Bus{id, fuelCost});
        }
        file.close();
    }
    catch (exception) {
        Reset();
        throw runtime_error("Something goes wrong");
    }

}

void BusStation::Reset()
{
    _numberBuses = 0;
    _buses.clear();
}

const Bus &BusStation::GetBus(const int &idBus) const {
    if(idBus > _numberBuses){
        throw runtime_error("Bus " + to_string(idBus) + " does not exists");
    }
    return _buses[idBus - 1];
}

void MapData::Load() {
    Reset();
    ifstream file;
    file.open(_mapFilePath.c_str());
    if (file.fail()){
        throw runtime_error("Something goes wrong");
    }
    try {
        string line = "";
        istringstream converter;

        getline(file, line); //comment
        getline(file, line);

        //BUS STOPS

        converter.str(line);
        converter >> _numberBusStops;
        converter.clear();

        _busStops.reserve(_numberBusStops);
        getline(file, line); //comment
        int id, lat, lon;
        string name;

        for (int i = 0; i < _numberBusStops; i++){
            getline(file, line);
            converter.str(line);
            converter >> id >> name >> lat >> lon;
            converter.clear();

            _busStops.push_back(BusStop{id, lat, lon, name});
        }

        //STREETS

        getline(file, line); //comment
        getline(file, line);

        converter.str(line);
        converter >> _numberStreets;
        converter.clear();

        _streets.reserve(_numberStreets);
        _streetFrom.reserve(_numberStreets);
        _streetTo.reserve(_numberStreets);
        getline(file, line); //comment

        int from, to, travelTime;
        for (int i = 0; i < _numberStreets; i++){
            getline(file, line);
            converter.str(line);
            converter >> id >> from >> to >> travelTime;
            converter.clear();

            _streets.push_back(Street{id, travelTime});
            _streetFrom.push_back(from);
            _streetTo.push_back(to);
        }

        //ROUTES
        getline(file, line); //comment
        getline(file, line);

        converter.str(line);
        converter >> _numberRoutes;
        converter.clear();

        _routes.reserve(_numberRoutes);
        _routeStreets.reserve(_numberRoutes);
        getline(file, line); //comment

        int numStreets, street;
        for (int i = 0; i < _numberRoutes; i++){
            getline(file, line);
            converter.str(line);
            converter >> id >> numStreets;

            _routes.push_back(Route{id, numStreets});
            _routeStreets.push_back(vector<int>());
            _routeStreets[i].reserve(numStreets);
            for (int j = 0; j < numStreets; j++){
                converter >> street;
                _routeStreets[i].push_back(street);
            }
            converter.clear();
        }
    }
    catch(exception){
        Reset();
        throw runtime_error("Something goes wrong");
    }


}

void MapData::Reset()
{
    _numberBusStops = 0;
    _numberStreets = 0;
    _numberRoutes = 0;
    _busStops.clear();
    _streets.clear();
    _routes.clear();
    _streetFrom.clear();
    _streetTo.clear();
    _routeStreets.clear();
}

const Street &MapData::GetRouteStreet(const int &idRoute, const int &streetPosition) const {
    if(idRoute > _numberRoutes){
        throw runtime_error("Route " + to_string(idRoute) + " does not exists");
    }
    if(streetPosition >= (int)_routeStreets[idRoute - 1].size()){
        throw runtime_error("Street at position " + to_string(streetPosition) + " does not exists");
    }
    return GetStreet(_routeStreets[idRoute - 1][streetPosition]);
}

const Route &MapData::GetRoute(const int &idRoute) const {
    if(idRoute > _numberRoutes){
        throw runtime_error("Route " + to_string(idRoute) + " does not exists");
    }
    return _routes[idRoute - 1];
}

const Street &MapData::GetStreet(const int &idStreet) const {
    if(idStreet > _numberStreets){
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");
    }
    return _streets[idStreet - 1];
}

const BusStop &MapData::GetStreetFrom(const int &idStreet) const {
    if(idStreet > _numberStreets){
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");
    }
    return GetBusStop(_streetFrom[idStreet - 1]);
}

const BusStop &MapData::GetStreetTo(const int &idStreet) const {
    if(idStreet > _numberStreets){
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");
    }
    return GetBusStop(_streetTo[idStreet - 1]);
}

const BusStop &MapData::GetBusStop(const int &idBusStop) const {
    if(idBusStop > _numberBusStops){
        throw runtime_error("BusStop " + to_string(idBusStop) + " does not exists");
    }
    return _busStops[idBusStop - 1];
}

int RoutePlanner::ComputeRouteTravelTime(const int &idRoute) const {
    const Route& route = _mapData.GetRoute(idRoute);
    int total = 0;
    for (int i = 0; i < route.NumberStreets; i++){
        total += _mapData.GetRouteStreet(route.Id,i).TravelTime;
    }
    return total;
}

int RoutePlanner::ComputeRouteCost(const int &idBus, const int &idRoute) const {
    const Bus& bus = _busStation.GetBus(idBus);
    const Route& route = _mapData.GetRoute(idRoute);
    float total = 0;
    for (int i = 0; i < route.NumberStreets; i++) {
        total += (float)BusAverageSpeed * ((float)_mapData.GetRouteStreet(route.Id, i).TravelTime / 3600) * (float)bus.FuelCost;
    }
    return total;
}

string MapViewer::ViewRoute(const int &idRoute) const {
    const Route& route = _mapData.GetRoute(idRoute);
    string result = to_string(route.Id) + ": ";
    for (int i = 0; i < route.NumberStreets; i++){
        result += _mapData.GetStreetFrom(_mapData.GetRouteStreet(idRoute, i).Id).Name + " -> ";
    }
    return result + _mapData.GetStreetTo(_mapData.GetRouteStreet(idRoute, route.NumberStreets - 1).Id).Name;
}

string MapViewer::ViewStreet(const int &idStreet) const {
    const BusStop& from = _mapData.GetStreetFrom(idStreet);
    const BusStop& to = _mapData.GetStreetTo(idStreet);
    return to_string(idStreet) + ": " + from.Name + " -> " + to.Name;
}

string MapViewer::ViewBusStop(const int &idBusStop) const {
    const BusStop& busStop = _mapData.GetBusStop(idBusStop);
    return busStop.Name + " (" + to_string((double)busStop.Latitude/10000) + ", " + to_string((double)busStop.Longitude/10000) + ")";
}

}
