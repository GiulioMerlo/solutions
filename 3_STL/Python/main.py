class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description

    def __lt__(self, other) -> bool:
        if self.Name < other.Name:
            return True
        return False


class Pizzeria:
    pass


class Pizza:
    def __init__(self, name: str):
        self.Name = name
        self.Ingredients = []

    def addIngredient(self, ingredient: Ingredient):
        self.Ingredients.append(ingredient)

    def numIngredients(self) -> int:
        return len(self.Ingredients)

    def computePrice(self, pizzeria: Pizzeria) -> int:
        total: int = 0
        for ingredient in self.Ingredients:
            for ing in pizzeria.IngList:
                if ingredient is ing.Name:
                    total += ing.Price
        return total


class Order:
    def __init__(self):
        self.Pizzas = []

    def getPizza(self, position: int) -> Pizza:
        if position <= 0 or position > len(self.Pizzas):
            raise Exception("Position passed is wrong")
        return self.Pizzas[position - 1]

    # def initializeOrder(self, numPizzas: int):
    #     pass

    def addPizza(self, pizza: Pizza):
        self.Pizzas.append(pizza)

    def numPizzas(self) -> int:
        return len(self.Pizzas)

    def computeTotal(self, pizzeria: Pizzeria) -> int:
        total: int = 0
        for pizza in self.Pizzas:
            for pizz in pizzeria.PizzaList:
                if pizza.Name is pizz.Name:
                    total += pizz.computePrice(pizzeria)
        return total


class Pizzeria:
    def __init__(self):
        self.IngList = []
        self.PizzaList = []
        self.OrderList = {}
        self.NextOrder: int = 1000

    def addIngredient(self, name: str, description: str, price: int):
        for ingredient in self.IngList:
            if ingredient.Name is name:
                raise Exception("Ingredient already inserted")
        self.IngList.append(Ingredient(name, price, description))

    def findIngredient(self, name: str) -> Ingredient:
        for ingredient in self.IngList:
            if ingredient.Name is name:
                return ingredient
        raise Exception("Ingredient not found")

    def addPizza(self, name: str, ingredients: []):
        for pizza in self.PizzaList:
            if pizza.Name is name:
                raise Exception("Pizza already inserted")
        self.PizzaList.append(Pizza(name))
        for ingredient in ingredients:
            self.PizzaList[-1].addIngredient(ingredient)

    def findPizza(self, name: str) -> Pizza:
        for pizza in self.PizzaList:
            if pizza.Name is name:
                return pizza
        raise Exception("Pizza not found")

    def createOrder(self, pizzas: []) -> int:
        if len(pizzas) == 0:
            raise Exception("Empty order")
        self.OrderList.update({self.NextOrder: Order()})
        for pizza in pizzas:
            self.OrderList[self.NextOrder].addPizza(Pizza(pizza))
        self.NextOrder += 1
        return self.NextOrder - 1

    def findOrder(self, numOrder: int) -> Order:
        if numOrder in self.OrderList:
            return self.OrderList[numOrder]
        raise Exception("Order not found")

    def getReceipt(self, numOrder: int, pizzeria: Pizzeria) -> str:
        result: str = ""
        if numOrder in self.OrderList:
            for pizza in self.OrderList[numOrder].Pizzas:
                for pizz in self.PizzaList:
                    if pizza.Name is pizz.Name:
                        result += "- " + pizz.Name + ", " + str(pizz.computePrice(pizzeria)) + " euro" + "\n"
            result += "  TOTAL: " + str(self.OrderList[numOrder].computeTotal(pizzeria)) + " euro" + "\n"
            return result
        raise Exception("Order not found")

    def listIngredients(self) -> str:
        result: str = ""
        self.IngList.sort()
        for ingredient in self.IngList:
            result += ingredient.Name + " - '" + ingredient.Description + "': " + str(ingredient.Price) + " euro" + "\n"
        return result

    def menu(self, pizzeria: Pizzeria) -> str:
        result: str = ""
        for pizza in self.PizzaList:
            result += pizza.Name + " (" + str(pizza.numIngredients()) + " ingredients): " + \
                      str(pizza.computePrice(pizzeria)) + " euro" + "\n"
        return result
