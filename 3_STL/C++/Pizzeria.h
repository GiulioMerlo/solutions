#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>
#include <set>
#include <unordered_map>

using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
    public:
      string Name;
      int Price;
      string Description;

      Ingredient(const string& name, const string& description, const int& price);
      Ingredient(const string& name);
      Ingredient(const Ingredient& ingredient);

      friend bool operator < (const Ingredient& a, const Ingredient& b){
          if (a.Name < b.Name) {
              return true;
          }
          return false;
      }

      friend bool operator == (const Ingredient& a, const Ingredient& b){
          if (a.Name == b.Name){
              return true;
          }
          return false;
      }
  };

  class Pizzeria;

  class Pizza {
    public:
      string Name;
      vector<Ingredient> Ingredients;

      Pizza(const string& name, const vector<string>& ingredients);

      void AddIngredient(const Ingredient& ingredient);
      int NumIngredients() const;
      int ComputePrice(const Pizzeria& pizzeria) const;
  };

  class Order {    
    public:
      vector<Pizza> Pizzas;

      void InitializeOrder(int numPizzas);
      void AddPizza(const Pizza& pizza);
      const Pizza& GetPizza(const int& position) const;
      int NumPizzas() const;
      int ComputeTotal(const Pizzeria& pizzeria) const;
  };

  class Pizzeria {
  private:
       set<Ingredient> IngList;
       unordered_map<string, Pizza> PizzaList;
       unordered_map<unsigned int, Order> OrderList;
       unsigned int NextOrder = 1000;

  public:
      void AddIngredient(const string& name,
                         const string& description,
                         const int& price);
      const Ingredient& FindIngredient(const string& name) const;
      void AddPizza(const string& name,
                    const vector<string>& ingredients);
      const Pizza& FindPizza(const string& name) const;

      int CreateOrder(const vector<string>& pizzas);
      const Order& FindOrder(const int& numOrder) const;
      string GetReceipt(const int& numOrder, const Pizzeria& pizzeria);
      string ListIngredients() const;
      string Menu(const Pizzeria& pizzeria) const;

      friend class Pizza;
      friend class Order;
  };
};

#endif // PIZZERIA_H
