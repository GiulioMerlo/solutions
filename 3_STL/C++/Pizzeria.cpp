#include "Pizzeria.h"

namespace PizzeriaLibrary {

///Constructor for the ingredient class
Ingredient::Ingredient(const string &name, const string &description, const int &price)
{
    Name = name;
    Description = description;
    Price = price;
}

///Alternative constructor for the ingredient class; description and price are not specified in this case
Ingredient::Ingredient(const string &name)
{
    Name = name;
    Description = "";
    Price = -1;
}

///Copy constructor for the ingredient class
Ingredient::Ingredient(const Ingredient &ingredient)
{
    Name = ingredient.Name;
    Description = ingredient.Description;
    Price = ingredient.Price;
}

///Add an ingredient to the pizzeria's list of ingredients
void Pizzeria::AddIngredient(const string &name, const string &description, const int &price)
{
    pair<set<Ingredient>:: iterator, bool> r;

    //insert returns a pair containing a iterator pointing to the position of the inserted element and true if it's successful
    //otherwise an iterators pointing to set.end and false are returned
    r = IngList.insert(Ingredient(name, description, price));

    //check success of insertion
    if(!r.second){
        throw runtime_error("Ingredient already inserted");
    }
}

///Search method; returns a reference to the searched ingredient. If the ingredient is not found raises an exception
const Ingredient &Pizzeria::FindIngredient(const string &name) const
{
    set<Ingredient> :: iterator it;

    //find returns an iterator pointing to the searched element or to set.end if the element is not found
    it = IngList.find(Ingredient(name));

    //check success
    if (it != IngList.end()){
        return *it;
    }
    throw runtime_error("Ingredient not found");
}

///Add a Pizza to the pizzeria's menu
void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients)
{
    pair <unordered_map<string, Pizza> :: iterator, bool> r;

    //write a new element in the menu. If succsessful returns a pair containing an iterator and a bool expressing the result
    //of the operation
    r = PizzaList.emplace(name, Pizza(name, ingredients));

    //check operation
    if (!r.second){
        throw runtime_error("Pizza already inserted");
    }
}

///Search method; returns a reference to the searched pizza. If the pizza is not found raises an exception
const Pizza &Pizzeria::FindPizza(const string &name) const
{
    //returns an iterator to the element if found
    auto it = PizzaList.find(name);

    //check
    if(it != PizzaList.end()){
        return it->second;
    }
    throw runtime_error("Pizza not found");
}

///Add an order to the pizzeria register
int Pizzeria::CreateOrder(const vector<string> &pizzas)
{
    pair <unordered_map <unsigned int, Order> :: iterator, bool> r;
    unsigned int n = pizzas.size();

    //check for empty order
    if (pizzas.size() == 0){
        throw runtime_error("Empty order");
    }

    //if order isn't empty add order and prepare it to be filled
    r = OrderList.emplace(NextOrder, Order());
    r.first->second.InitializeOrder(n);

    //Add pizzas to the order
    for (unsigned int i = 0; i < n; i++){
        r.first->second.AddPizza(Pizza(pizzas[i], vector<string>()));
    }

    //increase the next order number
    NextOrder++;

    //return added order number
    return r.first ->first;

}

///Search method; returns a reference to the searched order. If the order is not found raises an exception
const Order &Pizzeria::FindOrder(const int &numOrder) const
{
    //returns an iterator to the element if found
    auto it = OrderList.find(numOrder);

    //check
    if(it != OrderList.end()){
        return it -> second;
    }
    throw runtime_error("Order not found");
}

///Prints the recepit into a string
string Pizzeria::GetReceipt(const int &numOrder, const Pizzeria &pizzeria)
{
    string result = "";

    //search for the inputed order, if not found raise exception
    auto it = OrderList.find(numOrder);
    if(it == OrderList.end()){
        throw runtime_error("Order not found");
    }

    //print name and price of each pizza (using PizzaList attribute of pizzeria)
    for (unsigned int i = 0; i < it ->second.Pizzas.size(); i++){
        result += "- " + it ->second.Pizzas[i].Name + ", " + to_string(pizzeria.PizzaList.find(it ->second.Pizzas[i].Name) -> second.ComputePrice(pizzeria)) + " euro\n";
    }

    //print total price
    result +=  "  TOTAL: " + to_string(it -> second.ComputeTotal(pizzeria)) + " euro" + "\n";
    return result;

}

///prints the list of ingredients in alphabetical order
string Pizzeria::ListIngredients() const
{
    string result = "";

    //print each element of the list
    for (auto it = IngList.begin(); it != IngList.end(); it++){
        result += it -> Name + " - '" + it -> Description + "': " + to_string(it-> Price) + " euro" + "\n";
    }
    return result;
}

///Print the menu
string Pizzeria::Menu(const Pizzeria& pizzeria) const
{
    string result = "";
    for (auto i = PizzaList.begin(); i != PizzaList.end(); i++){
        result += i -> first + " (" + to_string(i -> second.NumIngredients()) + " ingredients): " + to_string(i -> second.ComputePrice(pizzeria)) + " euro" + "\n";
    }
    return result;
}

///Pizza class constructor
Pizza::Pizza(const string &name, const vector<string> &ingredients)
{
    Name = name;
    Ingredients.reserve(ingredients.size());
    for (unsigned int i = 0; i < ingredients.size(); i++){
        Ingredients.push_back(ingredients[i]);
    }
}

///Add an ingredient to the pizza object
void Pizza::AddIngredient(const Ingredient &ingredient)
{
    Ingredients.push_back(ingredient);
}

///Returns the number of ingredients on the pizza
int Pizza::NumIngredients() const
{
    return Ingredients.size();
}

///Calculates the price of each pizza
int Pizza::ComputePrice(const Pizzeria& pizzeria) const
{
    unsigned int total = 0;
    for (unsigned int i = 0; i < Ingredients.size(); i++){
        total += pizzeria.IngList.find(Ingredients[i]) -> Price;
    }
    return total;
}

///Prepares an order to be filled
void Order::InitializeOrder(int numPizzas)
{
    Pizzas.reserve(numPizzas);
}

///Adds a pizza to the order
void Order::AddPizza(const Pizza &pizza)
{
    Pizzas.push_back(pizza);
}

///Returns a reference to a pizza from a specified order
const Pizza &Order::GetPizza(const int &position) const
{
    if(1 <= position && position <= (int)Pizzas.size()){
        return Pizzas[position - 1];
    }
    throw runtime_error("Position passed is wrong");
}

///Returns the number of pizzas in the order
int Order::NumPizzas() const
{
    return Pizzas.size();
}

///Calculates the total price for each order
int Order::ComputeTotal(const Pizzeria &pizzeria) const
{
    unsigned int total = 0;
    for (unsigned int i = 0; i < Pizzas.size(); i++){
        total += pizzeria.PizzaList.find(Pizzas[i].Name) -> second.ComputePrice(pizzeria);
    }
    return total;
}

}
